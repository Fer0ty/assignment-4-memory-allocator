#include "test.h"

enum{
    HEAP_SIZE=8192,
    BLOCK_SIZE=1000,
    CONST_FOR_FIRST_TEST = 123,
    RAND_NUM_ONE = 1488,
    RAND_NUM_TWO = 1337,
    KB = 1024,
    FOUR_KB = 4096,
    MEM_BIG_BLOCK = 8000,
    MEM_SMALL_BLOCK = 500
    
};


static void print_message(char* str,...){
    printf("___ %s ___\n",str);

}

static void print_error(char* str){
    print_message("ERROR: %s",str);
}


static void print_heap(char* str,void *const heap){
    print_message(str);
    debug_heap(stdout,heap);
}


static void delete_heap(void *heap,size_t sz){
    munmap(heap,size_from_capacity((block_capacity){.bytes=sz}).bytes);
}

static struct block_header* get_header(void* contents){
    return (struct block_header*)
            ((uint8_t*) contents - offsetof(struct block_header, contents));
}

static int block_count(struct block_header* block){
    int count =0;
    struct block_header *iteration = block;
    while(iteration->next){
        if(!iteration->is_free){
            count+=1;
            iteration =iteration->next;
        }
    }
    return true;
}
struct block_header* block;
//simple malloc
void first_test(void) {
    print_message( "~~~~~~TEST~~~~~~~~~~~~1~~~~~~");
    void* heap_start = heap_init(REGION_MIN_SIZE);

    print_message("Heap state before _malloc(123):\n");
    debug_heap(stdout, heap_start);

    if (!_malloc(CONST_FOR_FIRST_TEST)) {
        print_error( "Test #1 failed\n");
        return;
    }

    print_message( "Heap state after _malloc(123):\n");
    debug_heap(stdout, heap_start);

    delete_heap(HEAP_START, REGION_MIN_SIZE);
    print_message( "Test #1 succeeded\n");
}


//free one block
void second_test(){
    print_message("~~~~~~TEST~~~~~~~~~~~~2~~~~~~");
    void* heap_start = heap_init(REGION_MIN_SIZE);

  print_message( "Heap state before _malloc's:\n");
  debug_heap(stdout, heap_start);

  void *before = _malloc(CONST_FOR_FIRST_TEST);
  void *to_be_freed = _malloc(RAND_NUM_ONE);
  void *after = _malloc(RAND_NUM_TWO);
  if (!before || !to_be_freed || !after) {
    print_message( "Test #2 failed\n");
    return;
  }

  print_message( "Heap state after _malloc's:\n");
  debug_heap(stdout, heap_start);

  _free(to_be_freed);

  print_message( "Heap state after _free:\n");
  debug_heap(stdout, heap_start);

  delete_heap(HEAP_START, REGION_MIN_SIZE);
  fprintf(stdout, "Test #2 succeeded\n\n");
}

//free two blocks
void third_test(){
    print_message("~~~~~~TEST~~~~~~~~~~~~3~~~~~~");

    void* heap = heap_init(FOUR_KB);
    if(!heap){
        print_error("Heap is not initialized");
        return;
    }
    print_message("Heap is initialized");

    void *mem1=_malloc(KB);
    void *mem2=_malloc(KB);
    void *mem3=_malloc(KB);

    if(!mem1||!mem2||!mem3){
        print_error("Memory is not allocated");
        return;
    }
    print_message("Memory is allocated");


    _free(mem1);
    print_message("Free 1-st block");

    _free(mem3);
    print_message("Free 3-rd block");

    if(!mem2){
        print_error("Release of the 1-st damaged the second");
    }

    delete_heap(HEAP_START,FOUR_KB);
    print_message("Test 3 success");
}

//memory expansion
void fourth_test(){
    print_message("~~~~~~TEST~~~~~~~~~~~~4~~~~~~");
    void* heap_start = heap_init(REGION_MIN_SIZE);
    print_error("Heap state before _malloc(8000):\n");
    debug_heap(stdout, heap_start);

    void* big = _malloc(MEM_BIG_BLOCK);

    if (!big) {
        print_error("Test #4 failed (couldn't allocate big block)\n");
        return;
    }

    print_message( "Heap state after _malloc(8000):\n");
    debug_heap(stdout, heap_start);

    void* small = _malloc(MEM_SMALL_BLOCK);

    if (!small || small != big + MEM_BIG_BLOCK) {
        print_error( "Test #4 failed (couldn't extend heap)\n");
        return;
    }

    print_message( "Heap state after _malloc(500):\n");
    debug_heap(stdout, heap_start);

    delete_heap(heap_start, REGION_MIN_SIZE);
    delete_heap(heap_start + REGION_MIN_SIZE, MEM_SMALL_BLOCK);
    print_message( "Test #4 succeeded\n");

}






void fifth_test(){
    print_message("~~~~~~TEST~~~~~~~~~~~~5~~~~~~");
    struct block_header* heap = (struct block_header*) heap_init(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    void *alloc = _malloc(REGION_MIN_SIZE + 1);
    if (size_from_capacity(heap->capacity).bytes < 1 + REGION_MIN_SIZE) {
        print_error( "Test #5 failed \n");
        _free(alloc);
        delete_heap(heap, REGION_MIN_SIZE + 1);
        return;
    }
    debug_heap(stdout, heap);
    _free(alloc);
    delete_heap(heap, REGION_MIN_SIZE + 1);
    print_message("Test #5 succeeded\n");
}


void run_tests(){
    printf("~~~~~~STARTING~~~~~TESTS~~~~~~");

    first_test();

    second_test();

    third_test();

    fourth_test();

    fifth_test();

    printf("~~~~~TESTS~~~~~~~~~~~END~~~~~~");
}

